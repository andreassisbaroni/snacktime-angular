import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Snack } from './snack';
import { Cart } from './cart';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private cartsUrl = 'http://localhost:8080/api/carts/';


  constructor(private http: HttpClient) { }

  addToCart(snack: Snack): Observable<Cart> {
    return this.http.post<Cart>(this.cartsUrl, snack);
  }

  getCart(): Observable<Cart> {
    return this.http.get<Cart>(this.cartsUrl);
  }
}
