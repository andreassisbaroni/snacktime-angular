import { Component, OnInit, Input } from '@angular/core';
import { Snack } from '../snack';
import { Cart } from '../cart';
import { SnackService } from '../snack.service';
import { CartComponent } from '../cart/cart.component';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-snack-list',
  templateUrl: './snack-list.component.html',
  styleUrls: ['./snack-list.component.css']
})
export class SnackListComponent implements OnInit {

  private _cart: Cart;

  @Input()
  set cart(cart: Cart) {
    this._cart = cart;
  }

  get cart(): Cart {
    return this._cart;
  }

  snacks: Snack[];

  constructor(private snackService: SnackService,
    private cartService: CartService,
    private cartComponent: CartComponent) { }

  ngOnInit() {
    this.getSnacks();
  }

  getSnacks(): void {
    this.snackService.getSnacks()
      .subscribe(data => this.snacks = data);
  }

  addSnackToCart(snack: Snack): void {
    this.cartService.addToCart(snack).subscribe(data => {
      this.cart = data;
      this.cartComponent.getCart();
    });

  }
}
