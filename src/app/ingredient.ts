export class Ingredient {
	id: number;
	description: string;
	price: number;

	constructor() {
	}
}