import { Ingredient } from './ingredient';
import { CartItem } from './cart.item';

export class CartItemIngredient {
    amount: number;
    cartItem: CartItem;
    ingredient: Ingredient;
    price: number;

    constructor() {
    }
}