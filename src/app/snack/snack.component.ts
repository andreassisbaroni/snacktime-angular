import { Component, OnInit } from '@angular/core';
import { Snack } from '../snack';
import { SnackService } from '../snack.service';

@Component({
  selector: 'app-snack',
  templateUrl: './snack.component.html',
  styleUrls: ['./snack.component.css']
})
export class SnackComponent implements OnInit {

  selectedSnack: Snack;

  constructor(private snackService: SnackService) { }

  ngOnInit() {
  }

  onSelect(snack: Snack): void {
    this.selectedSnack = snack;
  }

}
