import { Component, OnInit, ApplicationRef } from '@angular/core';
import { CartService } from '../cart.service';
import { Cart } from '../cart';
import { CartItem } from '../cart.item';
import { CartItemIngredient } from '../cart.item.ingredient';
import { CartItemService } from '../cart-item.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  currentCart: Cart = new Cart();

  constructor(private cartService: CartService,
    private cartItemService: CartItemService,
    private applicationRef: ApplicationRef) { }

  ngOnInit() {
    this.getCart();
  }

  getCart(): void {
    this.cartService.getCart()
      .subscribe(data => {
        this.currentCart = data;
      });
  }

  showLogCurrentCart(): void {
    console.log(this.currentCart);
  }

  recalculatePricesOfCurrentCart(): void {
    let cartTotalPrice: number = 0;
    this.currentCart.cartItems.forEach(function (cartItem: CartItem) {
      let cartItemTotalPrice: number = 0;
      cartItem.cartItemIngredients.forEach(function (cartItemIngredient: CartItemIngredient) {
        cartItemIngredient.price = cartItemIngredient.amount * cartItemIngredient.ingredient.price;
        cartItemTotalPrice = cartItemTotalPrice + cartItemIngredient.price;
      })
      cartItem.price = cartItemTotalPrice;
      cartTotalPrice = cartTotalPrice + cartItem.price;
    });
    this.currentCart.price = cartTotalPrice;
  }

  updateAllIngredients(cartItem: CartItem): void {
    this.cartItemService.updateAllIngredients(cartItem)
      .subscribe(data => {
        cartItem = data;
        this.getCart();
      });
  }

  deleteWithAllIngredients(cartItem: CartItem): void {
    this.cartItemService.deleteWithAllIngredients(cartItem).subscribe(data => this.getCart());
  }
}
