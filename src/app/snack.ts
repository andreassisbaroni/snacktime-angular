import { SnackIngredient } from './snack.ingredient';

export class Snack {
	id: number;
	description: string;
	snackIngredients: SnackIngredient[];
	price: number;

	constructor() {
		this.snackIngredients = [];
	}
}