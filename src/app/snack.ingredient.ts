import { Ingredient } from './ingredient';

export class SnackIngredient {
	amount: number;
	ingredient: Ingredient;
	price: number;

	constructor() {
	}
}