import { Snack } from './snack';
import { CartItemIngredient } from './cart.item.ingredient';

export class CartItem {
    id: number;
    snack: Snack;
    price: number;
    cartItemIngredients: CartItemIngredient[];

    constructor() {
        this.cartItemIngredients = [];
    }
}