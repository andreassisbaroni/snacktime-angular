import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';

import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { SnackComponent } from './snack/snack.component';
import { AppRoutingModule } from './/app-routing.module';
import { SnackListComponent } from './snack-list/snack-list.component';
import { IngredientComponent } from './ingredient/ingredient.component';
import { IngredientListComponent } from './ingredient-list/ingredient-list.component';
import { CartComponent } from './cart/cart.component';

@NgModule({
  declarations: [
    AppComponent,
    SnackComponent,
    SnackListComponent,
    IngredientComponent,
    IngredientListComponent,
    CartComponent
  ],
  imports: [
    BrowserModule, FormsModule, NgbModule.forRoot(), AppRoutingModule, HttpClientModule, AngularFontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
