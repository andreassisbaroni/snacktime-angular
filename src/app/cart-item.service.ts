import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { CartItem } from './cart.item';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CartItemService {

  private cartItemsUrl = 'http://localhost:8080/api/carts/items/';

  constructor(private http: HttpClient) { }

  updateAllIngredients(cartItem: CartItem): Observable<CartItem> {
    return this.http.put<CartItem>(this.cartItemsUrl + cartItem.id.toString(), cartItem);
  }

  deleteWithAllIngredients(cartItem: CartItem): Observable<CartItem> {
    return this.http.delete<CartItem>(this.cartItemsUrl + cartItem.id.toString());
  }

}
