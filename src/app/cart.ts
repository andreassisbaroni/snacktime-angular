import { CartItem } from './cart.item';

export class Cart {
	id: number;
	cartItems: CartItem[];
	price: number;

	constructor() {
		this.cartItems = [];
	}
}