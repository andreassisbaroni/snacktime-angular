import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';

import { Snack } from './snack';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class SnackService {

  private snacksUrl = 'http://localhost:8080/api/snacks/';

  constructor(private http: HttpClient) { }

  getSnacks(): Observable<Snack[]> {
    return this.http.get<Snack[]>(this.snacksUrl);
  }

}
